/**
 * Créations des variables générales nécessaire au fonctionnement du jeu:
 * Capture des boutons Jouer et Réinitialisation
 * Capture des valeurs dans les inputs Nom et Nombre de paire
 * Création du tableauErreur qui contiendra les messages d'erreur de validation
 * Création du tableaCarteAComparer qui permettra de comparer les cartes cliquées.
 * Création des variables pour la gestion des cartes
 * Création des variables pour la gestion du timer et capture de l'élément affichageTimer pour afficher en temps réel.
 */
const boutonJouer = document.getElementById('bouton-jouer')
const boutonReinitialisation = document.getElementById('bouton-reinitialisation')
const formulairePartie = document.getElementById('formulaire-partie')
const nomEntre = document.getElementById('input-nom')
const nombrePairesEntre = document.getElementById('input-nombre-paires')
const contenantUlErreur = document.getElementById('contenantUlErreur')
const tableauErreur = []
const tableauCarteAComparer = []
let carteSelectionnee = ""
let pairesTrouvees = 0
let pairesATrouvees = 0
let carte1 = 0
let carte2 = 0
// VARIABLES POUR TIMER
const startingMinutes = 5 //je peux choisir le temps que je veux!
let temps = startingMinutes * 60 //multiplier le nb de minutes entré pour obtenir les seconde
let compteurArret = 0
let compteurInterval = 0
const affichageTimer = document.getElementById('affichageTimer')


// Affectation des événements au bouton Jouer et Réinitialisation
boutonJouer.addEventListener('click', validerPartie)
boutonReinitialisation.addEventListener('click', reinitialisationTimer)

// Déclaration des fonctions


/** @description La fonction reinitialisationTimer arrête le timer de fin de partie et le décompte affiché
 *  Réinitialise aussi la partie et l'affichage du timer.
 */
function reinitialisationTimer() {
    clearInterval(temps = 0)
    reinitialisationPartie()
    arreterTimer()
    affichageTimer.innerHTML = (startingMinutes) + ": 00"
}

/** @description La fonction reinitialisationPartie réinitialise l'état du jeu:
 * les cartes affichées et les input sont réinitialisés
 * Le tabeauDeCartes est vidé ainsi que le UL contenant les messages d'erreur
 * Les pairesTrouvees et les pairesATrouvees sont réinitialiser pour permettre d'accéder à la victoire après réinitialisation.
 * La couleur des input du nombre de paires et du nom est remise à la couleur "blanc"
 */
function reinitialisationPartie() {
    document.getElementById("formulaire-partie").style.display = "block"  //montre le formulaire
    temps = startingMinutes * 60 //multiplier le nb de minutes entré pour obtenir les seconde
    tableauDeCartes.innerHTML = '' //vide le tableau de cartes
    contenantUlErreur.innerHTML = ''
    pairesTrouvees = 0
    pairesATrouvees = 0
    document.inscription.inputNombrePaires.style.backgroundColor = 'white' //réinitialise inputNombrePaires blanc
    document.inscription.inputNom.style.backgroundColor = 'white' //Réinitialise input-Nom blanc
}

/** @description La fonction obtenirParametres capture les valeur inscrite dans les input.
 * @return {nomJoueur et nombre de Cartes Entrée}
 */
function obtenirParametres() {
    let nombrePairesObtenu = nombrePairesEntre.value
    nombrePairesObtenu = parseInt(nombrePairesObtenu)
    let nomObtenu = nomEntre.value
    return { nomJoueur: nomObtenu, nombreCartesEntre: nombrePairesObtenu }
}

/** @description la fonction validerPartie réinitialise la partie et appelle la fonction genererCartes() si succès.
 * Si échec la fonction validerPartie affiche les messages d'erreur.
 */
function validerPartie() {
    reinitialisationPartie()
    tableauErreur.pop('') //retire les messages d'erreur dans le tableauErreur
    tableauErreur.pop('')//retire les messages d'erreur dans le tableauErreur
    //appelle la fonction obtenirParametres
    const validationParametresPartie = obtenirParametres()
    let nombrePairesAValider = validationParametresPartie.nombreCartesEntre
    //vérifie que le nombre de paires entrées est un chiffre compris entre 1 et 10
    if (isNaN(nombrePairesAValider) || nombrePairesAValider < 1 || nombrePairesAValider > 10) {
        document.inscription.inputNombrePaires.style.backgroundColor = 'red'
        tableauErreur.push('Veuillez entrer un nombre entre 1 et 10 s\'il vous plaît!')
    }

    // VALIDATION NOM
    let nomAValider = validationParametresPartie.nomJoueur
    // Déclaration du REGEX alphanumérique
    let regex = /[^a-z0-9\- ]+/i
    let nomRegex = regex.test(nomAValider)
    if ((nomRegex == true) || nomAValider == '') {
        document.inscription.inputNom.style.backgroundColor = 'red'
        tableauErreur.push('Vous devez entrer votre nom')
    }

    // AFFICHE contenantUlErreur
    if (tableauErreur.length > 0) {
        contenantUlErreur.innerHTML = ''
        //boucle dans le tableau pour intégré, s'il ya lieu, les messages d'erreur poussés dans les deux validation faites dans les "if" précédents
        for (let i = 0; i < tableauErreur.length; i++) {
            const li = document.createElement('li')
            const message = tableauErreur[i]
            const texte = document.createTextNode(message)
            li.appendChild(texte)
            contenantUlErreur.appendChild(li)
        }
    } else {
        //Déclare le nombre total de variable à trouvé, pour pouvoir obtenir la victoire.
        pairesATrouvees = nombrePairesAValider
        //Appel de la fonction genererCartes, crée nombre de paires.
        genererCartes()
    }
}

function genererCartes() {
    document.getElementById("formulaire-partie").style.display = "none"  //cache le formulaire
    demarrerTimer()
    const parametresPartie = obtenirParametres()
    const tableauCartes = []
    const tableauDeCartes = document.getElementById('tableauDeCartes')
    tableauDeCartes.innerHTML = ''
    for (let i = 0; i < parametresPartie.nombreCartesEntre; i++) {
        tableauCartes.push(i)
        tableauCartes.push(i)
    }

    while (tableauCartes.length > 0) {
        const index = Math.floor(Math.random() * tableauCartes.length)
        const numeroCarte = (tableauCartes[index])
        const carte = document.createElement('button')
        carte.style.fontSize = '2rem'
        carte.style.backgroundColor = 'lightgrey'
        carte.style.color = 'orange'
        carte.style.height = '100px'
        carte.style.width = '75px'
        carte.style.margin = '25px'
        carte.style.border = '5px solid'
        carte.style.borderImageSource = 'linear-gradient(145deg, #FC7519, #AB064D, #62037A)'
        carte.style.borderImageSlice = '1'
        carte.setAttribute('data-numero-carte', numeroCarte)
        carte.addEventListener('click', clicCarte)
        tableauDeCartes.appendChild(carte)
        tableauCartes.splice(index, 1)
    }
}

/** @description La fonction clicCarte exécute pousse la carte cliqué dans le tableauCarteAComparer.
 * Assigne cette carte dans la variable carte1 et la désactive.
 * carteSelectionne assigne le data-numero-carte (créé dans la fonction genrer carte) à la variable numeroMasque pour permettre l'affichage de la carte retourné.
 */
function clicCarte(e) {
    tableauCarteAComparer.push(e.target)
    if (tableauCarteAComparer.length < 2) {
        carte1 = $(this)
        carte1.attr("disabled", true)
    }
    carteSelectionnee = e.target
    const numeroMasque = carteSelectionnee.getAttribute('data-numero-carte')
    const numeroAffiche = document.createTextNode(numeroMasque)
    carteSelectionnee.appendChild(numeroAffiche)
    // Si carte retourné === 2, ajouté la 2e carte dans la variable carte2 pour pouvoir la désactivé.
    if (tableauCarteAComparer.length === 2) {
        carte2 = $(this)
        carte2.attr("disabled", true)

        /**Si les data-numero-carte sont égaux, ajouté 1 au nombre de paire trouvé 
        *  vider le contenu du tableau pour sélectionné de nouvelle carte et appeler la fonction viderCartesTournees.
        */
        if (tableauCarteAComparer[0].getAttribute('data-numero-carte') === tableauCarteAComparer[1].getAttribute('data-numero-carte')) {
            pairesTrouvees++
            viderCartesTournees()
            // Si le nombre de pairesTrouvees devient égal au nombre de pairesAtrouvees, appeler finPartieGagnee.
            if (pairesTrouvees === pairesATrouvees) {
                finPartieGagnee()
            }
            // Sinon, appeler la fonction appeler la fonction retournerCartesApres1seconde et viderCartesTournees juste après.
        } else {
            setTimeout(retournerCartesApres1Seconde, 1000)
            setTimeout(viderCartesTournees, 1001)
        }
    }
}

/** @description La fonction retournerCartesApres1Secondes retire l'attribue "disabled" des carte1 et carte2,
 * et vide le contenu html du data-numero-carte dans le tableauCarteAComparer pour masquer la carte.
 */
function retournerCartesApres1Seconde() {
    carte1.removeAttr("disabled")
    carte2.removeAttr("disabled")
    tableauCarteAComparer[0].innerHTML = ""
    tableauCarteAComparer[1].innerHTML = ""
}

/** @description La fonction viderCartesTournees retire les items pouvant contenir le html du data-numero-carte.
 */
function viderCartesTournees() {
    tableauCarteAComparer.pop()
    tableauCarteAComparer.pop()
}

/** @description La fonction demarrerTimer réajuste l'affichageTimer à l'écran et démarre le compteur qui rafraichira le timer visuel à chaque intervalle de 1 seconde.
 */
function demarrerTimer() {
    affichageTimer.innerHTML = (startingMinutes) + ": 00"
    compteurInterval = setInterval(updateAffichageTimer, 1000)
}

/** @description La fonction arreterTimer permet d'arrêter le compteur qui marque la finPartiePerdue
 * et arrête aussi le timer qui permet l'affichage à l'écran.
 * Réajuste l'affichageTimer à l'écran.
*/
function arreterTimer() {
    clearTimeout(compteurArret)
    clearTimeout(compteurInterval)
    affichageTimer.innerHTML = (startingMinutes) + ": 00"
}

/** @description La fonction updateAffichageTimer permet de raffraichir la valeur des minutes et la valeur des secondes pour soustraire 1 seconde, à toute les 1 secondes.
 *  Si l'affichage atteint 0, appel l'arrêt du compteur et la finPartiePerdue.
 */
function updateAffichageTimer() {
    if (temps <= 0) {
        clearTimeout(compteurArret)
        finPartiePerdue()
    }
    const minutes = Math.floor(temps / 60) //Divise la valeur de temps entré pour en extraire les minutes.
    let seconds = temps % 60 //retire le restant des secondes pour en extraire les secondes.
    seconds = seconds < 10 ? '0' + seconds : seconds //s'il reste moins que 10 secondes dans la partie des seconde à afficher au compteur, doubler l'unité 0 pour afficher 00
    affichageTimer.innerHTML = `${minutes}: ${seconds}` //Détermine le format d'affichage du compteur (minutes : secondes)
    temps-- //soustraire 1 à la valeur
}

/** @description La fonction finPartiePerdu arrête l'intervalle qui permet de raffraichir le temps et lui réassigne la valeur 0.
 * Affiche une alerte qui signale que le joueur a perdue la partie.
 * Appelle la fonction arreterTimer (pour empêcher une future alerte de fin de partie.
 * appel la fonction de réinitialisation de partie.
*/
function finPartiePerdue() {
    clearInterval(temps = 0)
    alert("Vous avez perdu! Votre temps est écoulé! Meilleur chance la prochaine fois!")
    arreterTimer()
    reinitialisationPartie()
}

/** @description La fonction finPartieGagnee arrête l'intervalle qui permet de raffraichir le temps et lui réassigne la valeur 0.
 * Affiche une alerte qui signale que le joueur a gagnée la partie.
 * Appelle la fonction arreterTimer (pour empêcher une future alerte de fin de partie.
 * appel la fonction de réinitialisation de partie.
*/
function finPartieGagnee() {
    clearInterval(temps = 0)
    alert("Victoire! Vous avez gagné!")
    arreterTimer()
    reinitialisationPartie()
}